Readme file for the CAPTCHA in THAI module
---------------------------------------------

captcha_in_thai.module is the basic CAPTCHA module.

Installation:
  Installation is like with all normal drupal modules:
  extract the 'captcha_in_thai' folder from the tar ball to the
  modules directory from your website.

Dependencies:
  The basic CAPTCHA module has no dependencies, nothing special is required.

Configuration:
  The configuration page is at /admin/config/people/captcha_in_thai,
  where you can configure the CAPTCHA in THAI module.
