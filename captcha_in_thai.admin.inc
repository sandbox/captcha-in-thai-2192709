<?php
/**
 * @file
 * Functionality and helper functions for CAPTCHA IN THAI administration.
 */


/**
 * Configuration form for CAPTCHA IN THAI.
 */
function captcha_in_thai_admin_settings() {
  $form = array();
  drupal_add_css(drupal_get_path('module', 'captcha_in_thai') . '/captcha_in_thai.css');
  drupal_add_css(drupal_get_path('module', 'captcha_in_thai') . '/farbtastic/farbtastic.css');
  drupal_add_js(drupal_get_path('module', 'captcha_in_thai') . '/farbtastic/farbtastic.js');
  $form['admin'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="admin">',
    '#suffix' => '</div>',
  );
  $form['admin']['admin_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('CAPTCHA IN THAI'),
    '#description' => t("ตั้งค่าการใช้งาน"),
  );
  $form['admin']['admin_form']['question_type'] = array(
    '#prefix' => '<div id="question_type">',
    '#type' => 'fieldset',
    '#title' => t('ประเภทคำถาม'),
    '#suffix' => '</div>',
  );
  $form['admin']['admin_form']['question_type']['captcha_in_thai_question_type1'] = array(
    '#type' => 'checkbox',
    '#title' => t('คำที่มักเขียนผิด'),
    '#default_value' => variable_get('captcha_in_thai_question_type1', TRUE),
  );
  $form['admin']['admin_form']['question_type']['captcha_in_thai_question_type2'] = array(
    '#type' => 'checkbox',
    '#title' => t('คำอ่าน'),
    '#default_value' => variable_get('captcha_in_thai_question_type2', TRUE),
  );
  $form['admin']['admin_form']['question_type']['captcha_in_thai_question_type3'] = array(
    '#type' => 'checkbox',
    '#title' => t('คำสุภาษิต'),
    '#default_value' => variable_get('captcha_in_thai_question_type3', TRUE),
  );
  $form['admin']['admin_form']['question_type']['captcha_in_thai_question_type4'] = array(
    '#type' => 'checkbox',
    '#title' => t('คำราชาศัพท์'),
    '#default_value' => variable_get('captcha_in_thai_question_type4', TRUE),
  );
  $form['admin']['admin_form']['font'] = array(
    '#prefix' => '<div id="font">',
    '#type' => 'fieldset',
    '#title' => t('รูปแบบตัวอักษร'),
    '#suffix' => '</div>',
  );
  $form['admin']['admin_form']['font']['captcha_in_thai_font_random'] = array(
    '#type' => 'radios',
    '#title' => t('สุ่มรูปแบบตัวอักษร'),
    '#options' => array(
      'yes' => t('เปิด'),
      'no' => t('ปิด'),
    ),
    '#default_value' => variable_get('captcha_in_thai_font_random', 'no'),
  );
  $form['admin']['admin_form']['font']['captcha_in_thai_font_select'] = array(
    '#type' => "select",
    '#title' => t("เลือกฟอนต์"),
    '#options' => array(
      "TH Srisakdi" => t("TH Srisakdi"),
      "TH Sarabun PSK" => t("TH Sarabun PSK"),
      "TH Niramit AS" => t("TH Niramit AS"),
      "TH Mali Grade 6" => t("TH Mali Grade 6"),
      "TH Krub" => t("TH Krub"),
      "TH KoHo" => t("TH KoHo"),
      "TH Kodchasan" => t("TH Kodchasan"),
      "TH Fah Kwang" => t("TH Fah Kwang"),
      "TH Charm of AU" => t("TH Charm of AU"),
      "TH Chamornman" => t("TH Chamornman"),
      "TH Chakra Petch" => t("TH Chakra Petch"),
      "TH Bai Jamjuree CP" => t("TH Bai Jamjuree CP"),
    ),
    '#default_value' => variable_get("captcha_in_thai_font_select", "TH Sarabun PSK"),
  );
  $form['admin']['admin_form']['interface'] = array(
    '#type' => 'fieldset',
    '#title' => t('การแสดงผล'),
    '#attributes' => array('id' => 'edit-captcha-description-wrapper'),
  );
  $form['admin']['admin_form']['interface']['captcha_in_thai_font_bold'] = array(
    '#type' => 'radios',
    '#title' => t('ความหนาตัวอักษร'),
    '#options' => array(
      'yes' => t('เปิด'),
      'no' => t('ปิด')),
    '#default_value' => variable_get('captcha_in_thai_font_bold', 'no'),
  );
  $form['admin']['admin_form']['interface']['captcha_in_thai_rand_color'] = array(
    '#type' => 'radios',
    '#title' => t('สุ่มสีตัวอักษรและสีพื้นหลัง'),
    '#options' => array(
      'yes' => t('เปิด'),
      'no' => t('ปิด')),
    '#default_value' => variable_get('captcha_in_thai_rand_color', 'no'),
  );
  $form['admin']['admin_form']['interface']['captcha_in_thai_distorted'] = array(
    '#type' => 'radios',
    '#title' => t('การบิดเบี้ยวของตัวอักษร'),
    '#options' => array(
      'high' => t('มาก'),
      'med' => t('กลาง'),
      'low' => t('น้อย'),
      'no' => t('ปิด'))	,
    '#default_value' => variable_get('captcha_in_thai_distorted', 'no'),
  );
  $form['admin']['admin_form']['interface']['captcha_in_thai_noise'] = array(
    '#type' => 'radios',
    '#title' => t('เส้นและจุด'),
    '#options' => array(
      'high' => t('มาก'),
      'med' => t('กลาง'),
      'low' => t('น้อย'),
      'no' => t('ปิด')),
    '#default_value' => variable_get('captcha_in_thai_noise', 'no'),
  );
  $form['admin']['admin_form']['color'] = array(
    '#type' => 'fieldset',
    '#title' => t('การเปิดใช้งาน'),
  );

  // Font and background color for login part.
  $form['admin']['admin_form']['color']['login'] = array(
    '#type' => 'fieldset',
    '#title' => t('หน้า login'),
  );
  $form['admin']['admin_form']['color']['login']['captcha_in_thai_openlogin'] = array(
    '#type' => 'radios',
    '#options' => array(
      'user_login_block' => t('เปิด'),
      'close' => t('ปิด')),
    '#default_value' => variable_get('captcha_in_thai_openlogin', 'close'),
  );
  $form['admin']['admin_form']['color']['login']['captcha_in_thai_fontlogin'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#title' => t('สีตัวอักษร'),
    '#default_value' => variable_get('captcha_in_thai_fontlogin', '#000000'),
    '#description' => '<div id="captcha_in_thai_fontlogin"></div>',
  );
  $form['admin']['admin_form']['color']['login']['login_font'] = _captcha_in_thai_js('#captcha_in_thai_fontlogin', '#edit-captcha-in-thai-fontlogin');
  $form['admin']['admin_form']['color']['login']['captcha_in_thai_bglogin'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#title' => t('สีพื้นหลัง'),
    '#default_value' => variable_get('captcha_in_thai_bglogin', '#64dcdc'),
    '#description' => '<div id="captcha_in_thai_bglogin"></div>',
  );
  $form['admin']['admin_form']['color']['login']['login_bg'] = _captcha_in_thai_js('#captcha_in_thai_bglogin', '#edit-captcha-in-thai-bglogin');

  // Font and background color for lost password part.
  $form['admin']['admin_form']['color']['lostpw'] = array(
    '#type' => 'fieldset',
    '#title' => t('หน้าลืมรหัสผ่าน'),
  );
  $form['admin']['admin_form']['color']['lostpw']['captcha_in_thai_openlost'] = array(
    '#type' => 'radios',
    '#options' => array(
      'user_pass' => t('เปิด'),
      'close' => t('ปิด')),
    '#default_value' => variable_get('captcha_in_thai_openlost', 'close'),
  );
  $form['admin']['admin_form']['color']['lostpw']['captcha_in_thai_fontlostpw'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#title' => t('สีตัวอักษร'),
    '#default_value' => variable_get('captcha_in_thai_fontlostpw', '#000000'),
    '#description' => '<div id="captcha_in_thai_fontlostpw"></div>',
  );
  $form['admin']['admin_form']['color']['lostpw']['lostpw_font'] = _captcha_in_thai_js('#captcha_in_thai_fontlostpw', '#edit-captcha-in-thai-fontlostpw');
  $form['admin']['admin_form']['color']['lostpw']['captcha_in_thai_bglostpw'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#title' => t('สีพื้นหลัง'),
    '#default_value' => variable_get('captcha_in_thai_bglostpw', '#64dcdc'),
    '#description' => '<div id="captcha_in_thai_bglostpw"></div>',
  );
  $form['admin']['admin_form']['color']['lostpw']['lostpw_bg'] = _captcha_in_thai_js('#captcha_in_thai_bglostpw', '#edit-captcha-in-thai-bglostpw');

  // Font and background color for comment part.
  $form['admin']['admin_form']['color']['comment'] = array(
    '#type' => 'fieldset',
    '#title' => t('หน้าแสดงความคิดเห็น'),
  );
  $form['admin']['admin_form']['color']['comment']['captcha_in_thai_opencomment'] = array(
    '#type' => 'radios',
    '#options' => array(
      'comment_node_page_form' => t('เปิด'),
      'close' => t('ปิด')),
    '#default_value' => variable_get('captcha_in_thai_opencomment', 'close'),
  );
  $form['admin']['admin_form']['color']['comment']['captcha_in_thai_fontcomment'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#title' => t('สีตัวอักษร'),
    '#default_value' => variable_get('captcha_in_thai_fontcomment', '#000000'),
    '#description' => '<div id="captcha_in_thai_fontcomment"></div>',
  );
  $form['admin']['admin_form']['color']['comment']['comment_font'] = _captcha_in_thai_js('#captcha_in_thai_fontcomment', '#edit-captcha-in-thai-fontcomment');
  $form['admin']['admin_form']['color']['comment']['captcha_in_thai_bgcomment'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#title' => t('สีพื้นหลัง'),
    '#default_value' => variable_get('captcha_in_thai_bgcomment', '#64dcdc'),
    '#description' => '<div id="captcha_in_thai_bgcomment"></div>',
  );
  $form['admin']['admin_form']['color']['comment']['comment_bg'] = _captcha_in_thai_js('#captcha_in_thai_bgcomment', '#edit-captcha-in-thai-bgcomment');

  // Font and background color for register part.
  $form['admin']['admin_form']['color']['register'] = array(
    '#type' => 'fieldset',
    '#title' => t('หน้าสมัครสมาชิก'),
  );
  $form['admin']['admin_form']['color']['register']['captcha_in_thai_openregis'] = array(
    '#type' => 'radios',
    '#options' => array(
      'user_register_form' => t('เปิด'),
      'close' => t('ปิด')),
    '#default_value' => variable_get('captcha_in_thai_openregis', 'close'),
  );
  $form['admin']['admin_form']['color']['register']['captcha_in_thai_font_reg'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#title' => t('สีตัวอักษร'),
    '#default_value' => variable_get('captcha_in_thai_font_reg', '#000000'),
    '#description' => '<div id="captcha_in_thai_font_reg"></div>',
  );
  $form['admin']['admin_form']['color']['register']['reg_font'] = _captcha_in_thai_js('#captcha_in_thai_font_reg', '#edit-captcha-in-thai-font-reg');
  $form['admin']['admin_form']['color']['register']['captcha_in_thai_bg_reg'] = array(
    '#type' => 'textfield',
    '#size' => '20',
    '#title' => t('สีพื้นหลัง'),
    '#default_value' => variable_get('captcha_in_thai_bg_reg', '#64dcdc'),
    '#description' => '<div id="captcha_in_thai_bg_reg"></div>',
  );
  $form['admin']['admin_form']['color']['register']['reg_bg'] = _captcha_in_thai_js('#captcha_in_thai_bg_reg', '#edit-captcha-in-thai-bg-reg');
  $form = system_settings_form($form);
  $form['#submit'][] = 'captcha_in_thai_admin_settings_submit';

  return $form;

}


/**
 * Submit function for CAPTCHA IN THAI configuration form.
 */
function captcha_in_thai_admin_settings_submit($form, &$form_state) {
  if ($form == NULL) {
    drupal_set_message(t('The CAPTCHA IN THAI settings have been saved.'));
  }
}

/**
 * Javascripts function for CAPTCHA IN THAI configuration form.
 */
function _captcha_in_thai_js($id, $arrname) {
  $tmp = array();
  $tmp = array(
    '#type' => 'item',
    '#description' => "<script type='text/javascript'> $(document).ready(function() { $('$id').hide(); $('$id').farbtastic('$arrname');$('$arrname').click(function() { $('$id').slideToggle()});});</script>",
  );

  return $tmp;
}
