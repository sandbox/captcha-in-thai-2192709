<?php
/**
 * @file
 * General CAPTCHA IN THAI functionality and helper functions.
 */

/**
 * Helper function to insert a CAPTCHA IN THAI element in a form.
 */
function _captcha_in_thai_insert_captcha_element(&$form, $placement, $captcha_element) {
  // Get path, target and target weight or use defaults if not available.
  $target_key = isset($placement['key']) ? $placement['key'] : NULL;
  $target_weight = isset($placement['weight']) ? $placement['weight'] : NULL;
  $path = isset($placement['path']) ? $placement['path'] : array();

  // Walk through the form along the path.
  $form_stepper = &$form;
  foreach ($path as $step) {
    if (isset($form_stepper[$step])) {
      $form_stepper = &$form_stepper[$step];
    }
    else {
      // Given path is invalid: stop stepping and
      // continue in best effort (append instead of insert).
      $target_key = NULL;
      break;
    }
  }

  // If no target is available just append the CAPTCHA element to the container.
  if ($target_key == NULL || !array_key_exists($target_key, $form_stepper)) {
    // Optionally, set weight of CAPTCHA element.
    if ($target_weight != NULL) {
      $captcha_element['#weight'] = $target_weight;
    }
    $form_stepper['captcha_in_thai'] = $captcha_element;
  }
  else {
    // If target has a weight: set weight of CAPTCHA element a bit smaller
    // and just append the CAPTCHA: sorting will fix the ordering anyway.
    if ($target_weight != NULL) {
      $captcha_element['#weight'] = $target_weight - .1;
      $form_stepper['captcha_in_thai'] = $captcha_element;
    }
    else {
      // If we can't play with weights.
      // Insert the CAPTCHA element at the right position.
      // Because PHP lacks a function for this (array_splice() comes close,
      // but it does not preserve the key of the inserted element),
      // we do it by hand chop of the end.
      // append the CAPTCHA element and put the end back.
      $offset = array_search($target_key, array_keys($form_stepper));
      $end = array_splice($form_stepper, $offset);
      $form_stepper['captcha_in_thai'] = $captcha_element;
      foreach ($end as $k => $v) {
        $form_stepper[$k] = $v;
      }
    }
  }
}
/**
 * Function to get a CAPTCHA placement element.
 */
function _captcha_in_thai_get_captcha_placement($form_id, $form) {
  // Get CAPTCHA placement map from cache. Two levels of cache:
  // static variable in this function and storage in the variables table.
  static $placement_map = NULL;
  // Try first level cache.
  if ($placement_map === NULL) {
    // If first level cache missed: try second level cache.
    $placement_map = variable_get('captcha_placement_map_cache', NULL);

    if ($placement_map === NULL) {
      // If second level cache missed: initialize the placement map
      // and let other modules hook into this
      // with the hook_captcha_placement_map hook.
      $placement_map = module_invoke_all('captcha_placement_map');
    }
  }

  // Query the placement map.
  if (array_key_exists($form_id, $placement_map)) {
    $placement = $placement_map[$form_id];
  }
  // If no placement info is available in placement map
  // make a best effort guess.
  else {
    // If there is an "actions" button group
    // , a good placement is just before that.
    if (isset($form['actions']) && isset($form['actions']['#type']) && $form['actions']['#type'] === 'actions') {
      $placement = array(
        'path' => array(),
        'key' => 'actions',
        // #type 'actions' defaults to 100.
        'weight' => (isset($form['actions']['#weight']) ? $form['actions']['#weight'] - 1 : 99),
      );
    }
    else {
      // Search the form for buttons and guess placement from it.
      $buttons = _captcha_search_buttons($form);
      if (count($buttons)) {
        // Pick first button.
        // TODO: make this more sofisticated? Use cases needed.
        $placement = $buttons[0];
      }
      else {
        // Use NULL when no buttons were found.
        $placement = NULL;
      }
    }

    // Store calculated placement in cache.
    $placement_map[$form_id] = $placement;
    variable_set('captcha_placement_map_cache', $placement_map);
  }

  return $placement;
}

/**
 * Get question.
 */
function _captcha_in_thai_question() {

  $query = "SELECT count(*) as sum FROM ct_question WHERE";
  $is_first_type = TRUE;

  if (TRUE == variable_get('captcha_in_thai_question_type1')) {
    if ($is_first_type == FALSE) {
      $query .= " OR";
    }
    $query .= " type_id = 1 ";
    $is_first_type = FALSE;
  }

  if (TRUE == variable_get('captcha_in_thai_question_type2')) {
    if ($is_first_type == FALSE) {
      $query .= " OR";
    }
    $query .= " type_id = 2 ";
    $is_first_type = FALSE;
  }

  if (TRUE == variable_get('captcha_in_thai_question_type3')) {
    if ($is_first_type == FALSE) {
      $query .= " OR";
    }
    $query .= " type_id = 3 ";
    $is_first_type = FALSE;
  }

  if (TRUE == variable_get('captcha_in_thai_question_type3')) {
    if ($is_first_type == FALSE) {
      $query .= " OR";
    }
    $query .= " type_id = 4 ";
    $is_first_type = FALSE;
  }

  $query .= "ORDER BY RAND() LIMIT 1";

  if (!db_query($query)) {
    die('Invalid query');
  }
  else {
    $row = db_query($query)->fetchObject();;
    $sum = $row->sum;
    $id = rand(1, $sum);
    $user_ip = ip_address();

    $query = "SELECT count(*) as numrows FROM ct_user_question WHERE user_ip = '$user_ip' ";
    $check = db_query($query)->fetchField();;
    if ($check['numrows'] > 0) {
      $query = "DELETE FROM ct_user_question WHERE user_ip = '$user_ip'";
      db_query($query);
    }

    $query = "INSERT INTO ct_user_question ( user_ip, question_id )
    VALUES ( '$user_ip','$id' )";
    db_query($query);

    $query = "SELECT id FROM ct_user_question WHERE user_ip = '$user_ip'";
    $temp = db_query($query)->fetchField();;
    $str_id = $temp;
  }
  return $str_id;
}

/**
 * Create distortes in the image.
 */
function _captcha_in_thai_distorted($width, $height, &$image, $status) {
  $val['distorted']['high'] = array(
    'period' => rand(8, 10),
    'amp' => rand(7, 9),
  );
  $val['distorted']['med'] = array(
    'period' => rand(5, 7),
    'amp' => rand(5, 6),
  );
  $val['distorted']['low'] = array('period' => rand(2, 4) , 'amp' => rand(2, 4));
  $width2 = $width * 2;
  $height2 = $height * 2;
  $period = $val['distorted'][$status]['period'];
  $amp = $val['distorted'][$status]['amp'];
  $image2 = imagecreatetruecolor($width2, $height2);
  imagecopyresampled($image2, $image, 0, 0, 0, 0, $width2, $height2, $width, $height);
  for ($i = 0; $i < $width2; $i += 2) {
    imagecopy($image2, $image2, $i - 2, sin($i / $period) * $amp, $i, 0, 2, $height2);
  }
  imagecopyresampled($image, $image2, 0, 0, 0, 0, $width, $height, $width2, $height2);
  imagedestroy($image2);
}

/**
 * Create noise in the image.
 */
function _captcha_in_thai_noise(&$image, $status, $width, $height, $font_line_color, $bg) {
  $val['noise']['high'] = array(
    'dot' => $width * $height / 10,
    'line' => 20,
    'bgline' => 10,
  );
  $val['noise']['med'] = array(
    'dot' => $width * $height / 20,
    'line' => 10,
    'bgline' => 5,
  );
  $val['noise']['low'] = array(
    'dot' => $width * $height / 40,
    'line' => 5,
    'bgline' => 5,
  );
  // สร้างจุด
  for ($i = 0; $i < $val['noise'][$status]['dot']; $i++) {
    $cx = rand(0, $width);
    $cy = rand(0, $height);
    imageellipse($image, $cx, $cy, 1, 1, $font_line_color);
  }
  // สร้างเส้นสีดำ
  for ($i = 0; $i < $val['noise'][$status]['line']; $i++) {
    $x1 = rand(0, $width / 2);
    $x2 = rand($width / 2 - ($width / 5), $width);
    $y1 = rand(0 + ($height / 5), $height - ($height / 5));
    $y2 = rand($height / 2 - ($height / 5), $height);
    imageline($image, $x1, $y1, $x2, $y1, $font_line_color);
  }
  // สร้างเส้นสีเเขียว
  for ($i = 0; $i < $val['noise'][$status]['bgline']; $i++) {
    $x1 = rand(0, $width / 2);
    $x2 = rand($width / 2 - ($width / 5), $width);
    $y1 = rand(0 + ($height / 5), $height - ($height / 5));
    $y2 = rand(0 + ($height / 5), $height - ($height / 5));
    imageline($image, $x1, $y1, $x2, $y2, $bg);
  }
}

/**
 * Create the image for CAPTCHA.
 */
function captcha_in_thai_image($form_id) {
  putenv('GDFONTPATH=' . realpath('.'));
  $width = 200;
  $height = 125;
  $xfont = 40;
  $yfont = 70;
  $angle = rand(-10, 10);
  $font_size = 25;
  if (variable_get('captcha_in_thai_font_random') == 'yes') {
    $font_name = db_query("SELECT fontname FROM {ct_fonts} ORDER BY RAND() LIMIT 1")->fetchField();;
  }
  elseif (variable_get('captcha_in_thai_font_random') == 'no') {
    $font_name = variable_get('captcha_in_thai_font_select');
  }
  $fonts = drupal_get_path('module', 'captcha_in_thai') . '/font/' . $font_name . '.ttf';

  if (variable_get('captcha_in_thai_rand_color') == 'yes') {
    $r = rand(0, 15);
    $g = rand(0, 15);
    $b = rand(0, 15);
    $bg_color = dechex($r) . dechex($r) . dechex($g) . dechex($g) . dechex($b) . dechex($b);
    $font_color = "#" . _captcha_in_thai_oppositehex($bg_color);
    $bg_color = "#" . $bg_color;
  }
  elseif (variable_get('captcha_in_thai_rand_color') == 'no') {
    $colors = captcha_in_thai_color($form_id);
    $font_color = $colors[0];
    $bg_color = $colors[1];
  }
  $str_id = _captcha_in_thai_question();
  $query = "  SELECT question 
                FROM {ct_question}
                WHERE question_id 
                IN  (   SELECT question_id 
                        FROM ct_user_question 
                        WHERE id = $str_id
                    )";
  $str = db_query($query)->fetchField();;

  $image = imagecreate($width, $height);
  $bg = imagecolorallocate($image, hexdec(substr($bg_color, 1, 2)), hexdec(substr($bg_color, 3, 2)), hexdec(substr($bg_color, 5, 2)));
  $font_line_color = imagecolorallocate($image, hexdec(substr($font_color, 1, 2)), hexdec(substr($font_color, 3, 2)), hexdec(substr($font_color, 5, 2)));
  imagettftext($image, $font_size, $angle, $xfont, $yfont, $font_line_color, $fonts, $str);
  if (variable_get('captcha_in_thai_distorted') != 'no') {
    $status = variable_get('captcha_in_thai_distorted');
    _captcha_in_thai_distorted($width, $height, $image, $status);
  }

  if (variable_get('captcha_in_thai_noise') != 'no') {
    $status = variable_get('captcha_in_thai_noise');
    _captcha_in_thai_noise($image, $status, $width, $height, $font_line_color, $bg);
  }
  header("Content-type:image/png");
  imagepng($image);
  imagedestroy($image);
  exit();
}

/**
 * Get color from form_id.
 */
function captcha_in_thai_color($form_id) {
  if ($form_id == 'user_login_block' || $form_id == 'user_login') {
    $color[0] = variable_get('captcha_in_thai_fontlogin');
    $color[1] = variable_get('captcha_in_thai_bglogin');
    return $color;
  }
  if ($form_id == 'user_register_form') {
    $color[0] = variable_get('captcha_in_thai_font_reg');
    $color[1] = variable_get('captcha_in_thai_bg_reg');
    return $color;
  }
  if ($form_id == 'user_pass') {
    $color[0] = variable_get('captcha_in_thai_fontlostpw');
    $color[1] = variable_get('captcha_in_thai_bglostpw');
    return $color;
  }
  if ($form_id == 'comment_node_page_form') {
    $color[0] = variable_get('captcha_in_thai_fontcomment');
    $color[1] = variable_get('captcha_in_thai_bgcomment');
    return $color;
  }
}

/**
 * Find opposite hex number.
 */
function _captcha_in_thai_oppositehex($color) {
  $r = dechex(255 - hexdec(substr($color, 0, 2)));
  $r = (strlen($r) > 1) ? $r : '0' . $r;
  $g = dechex(255 - hexdec(substr($color, 2, 2)));
  $g = (strlen($g) > 1) ? $g : '0' . $g;
  $b = dechex(255 - hexdec(substr($color, 4, 2)));
  $b = (strlen($b) > 1) ? $b : '0' . $b;
  return $r . $g . $b;
}
